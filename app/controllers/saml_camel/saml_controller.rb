# frozen_string_literal: true

require_dependency 'saml_camel/application_controller'
module SamlCamel
  # handles SamlCamel SP
  class SamlController < ApplicationController
    include SamlCamel::SamlService
    skip_before_action :verify_authenticity_token, only: %i[consume logout raw_response]
    before_action :saml_protect, only: [:attr_check]

    def attr_check; end
    # consumes the saml response from the IDP
    # TODO break out into separate methods if possible
    # TODO rubocop also suggests to many assignments going on in consume

    def consume # rubocop:disable Metrics/MethodLength, Metrics/AbcSize
      permit_key = session[:saml_session_id].to_sym
      user_cache = Rails.cache.fetch(permit_key)
      unless cache_available?(user_cache)
        raise 'Unable to access cache. Ensure cache is configured according to documentation.'
      end
      redirect_path = user_cache[:redirect_url]
      sp = SamlCamel::ServiceProvider.new(
        cache_permit_key: permit_key, saml_attributes: session[:saml_attributes]
      )
      ol_response = SamlCamel::ServiceProvider.ol_response(params[:SAMLResponse])

      if sp.validate_idp_response(ol_response, request.remote_ip)
        # authorize_success, log the user
        session[:saml_response_success] = true
        sp.set_saml_session_lifetime
        session[:sp_session] = Time.now
        session[:saml_attributes] = SamlCamel::Transaction.map_attributes(ol_response.attributes)
        SamlCamel::Logging.successful_auth(session[:saml_attributes])

        redirect_to redirect_path
      else # otherwise list out the errors in the response
        if session[:saml_session_id]
          permit_key = session[:saml_session_id].to_sym
          Rails.cache.delete(permit_key)
          session[:saml_session_id] = nil
        end
        session[:sp_session] = nil
        session[:saml_response_success] = false
        # response.errors
        SamlCamel::Logging.auth_failure(ol_response.errors)

        redirect_to action: 'failure', locals: { errors: ol_response.errors }
      end
    rescue StandardError => e
      if session[:saml_session_id]
        permit_key = session[:saml_session_id].to_sym
        Rails.cache.delete(permit_key)
      end
      session[:saml_response_success] = false
      session[:saml_session_id] = nil
      session[:sp_session] = nil

      SamlCamel::Logging.auth_failure(e)
      redirect_to action: 'failure', locals: { errors: e }
    end

    # route to show saml failures
    def failure
      @error = params[:locals][:errors]
    end

    # convinence route to see attributes that are coming through
    def index
      @attributes = session[:saml_attributes]
    end

    # kills SP session and redirects to IDP to kill idp session
    def logout
      SamlCamel::Logging.logout(session[:saml_attributes])
      session[:saml_attributes] = nil
      session[:sp_session] = nil

      # return_url = SamlCamel::Transaction.logout #this methods logs the user
      # out of the IDP, and returns a url to be redirected to
      redirect_to SP_SETTINGS['settings']['logout_url']
    end

    # generate a metadata page that you may need to share with an IDP
    def metadata
      settings = SamlCamel::Transaction.saml_settings
      meta = OneLogin::RubySaml::Metadata.new
      render xml: meta.generate(settings)
    end

    # meant to force authn and then be redirected to raw response to view raw decrypted response
    def force_authn
      saml_request_url = SamlCamel::ServiceProvider.new.generate_saml_request(request, force_authn: true)
      redirect_to(saml_request_url)
    end

    def raw_response
      ol_response = SamlCamel::ServiceProvider.ol_response(params[:SAMLResponse], raw_response: true)
      render xml: ol_response.decrypted_document.to_s if ol_response.is_valid?
    end

    private

    def saml_settings
      SamlCamel::Transaction.saml_settings
    end
  end
end
