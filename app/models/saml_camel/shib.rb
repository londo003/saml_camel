# frozen_string_literal: true

module SamlCamel
  # handle shib attributes
  class Shib
    if SP_SETTINGS.dig('settings','shib_module')
      ATTRIBUTE_MAP = JSON.parse(File.read('config/saml/shibboleth.json'))
    end

    def self.attributes(request)
      attrs = {}
      ATTRIBUTE_MAP.each do |header_key, new_key|
        attrs[new_key] = request.env.dig(header_key)
      end
      attrs
    end

    # sets shib headers directly
    def self.set_headers(request: nil, identity: nil)
      identity.each { |k, v| request.env[k] = v }
    end
  end
end
