# frozen_string_literal: true

require 'saml_camel/engine'

# main logic for non initialized SAML camel methods
module SamlCamel
  begin
    SP_SETTINGS = JSON.parse(File.read("config/saml/#{Rails.env}/settings.json"))
    SP_DEBUG = SP_SETTINGS['settings']['debug']
  rescue StandardError # rubocop:disable Lint/HandleExceptions
    # rescue othewise the generator fails
  end

  # builds saml requests and decrypts saml responses
  module Transaction
    begin
      IDP_CERT = File.read("config/saml/#{Rails.env}/idp_certificate.crt")
      SP_CERT = File.read("config/saml/#{Rails.env}/saml_certificate.crt")
      SP_KEY = File.read("config/saml/#{Rails.env}/saml_key.key")
    rescue StandardError # rubocop:disable Lint/HandleExceptions
      # rescue othewise the generator fails
    end

    def self.map_attributes(sp_attributes)
      attr_map = SP_SETTINGS['attribute_map']
      mapped_attributes = {}

      sp_attributes.each do |sp_attribute, value|
        sp_attribute = attr_map[sp_attribute] || value
        mapped_attributes[sp_attribute] = value
      end
      mapped_attributes
    end

    def self.saml_settings(raw_response: false)
      sp_settings = SP_SETTINGS['settings']

      settings = OneLogin::RubySaml::Settings.new
      if raw_response
        settings.assertion_consumer_service_url = sp_settings['raw_response_acs']
        settings.force_authn = '1'
      else
        settings.assertion_consumer_service_url = sp_settings['acs']
      end
      settings.issuer                         = sp_settings['entity_id']
      settings.idp_sso_target_url             = sp_settings['sso_url']

      # certificate to register with IDP and key to decrypt
      settings.certificate = SP_CERT

      # certificate to decrypt SAML response
      settings.private_key = SP_KEY

      # certificate to verify IDP signature
      settings.idp_cert = IDP_CERT

      # inidcates SP wants assertions to be signed
      settings.security[:want_responses_signed] = true

      settings
    end
  end

  # handles logging throughout SP
  module Logging
    begin
      PRIMARY_ID = SP_SETTINGS['settings']['primary_id']
      SHOULD_LOG = SP_SETTINGS['settings']['saml_logging']
      LOGGER = Logger.new('log/saml.log')
    rescue StandardError # rubocop:disable Lint/HandleExceptions
      # rescue othewise the generator fails
    end

    def self.auth_failure(error_context)
      LOGGER.error("An error occured during authentication. #{error_context}") if SHOULD_LOG
      LOGGER.error("Backtrace: \n\t\t#{error_context.backtrace.join("\n\t\t")}") if SHOULD_LOG
    rescue StandardError
      LOGGER.debug('Unknown Error During auth_failure logging.') if SHOULD_LOG
    end

    def self.bad_ip(saml_attrs, request_ip, current_ip)
      if SHOULD_LOG
        LOGGER.info("Bad IP address for #{saml_attrs[PRIMARY_ID]}. IP at SAML
 request #{request_ip} | IP presented #{current_ip}")
      end
    rescue StandardError
      LOGGER.debug('Unknown Error During relay state logging. IP check') if SHOULD_LOG
    end

    def self.debug(message)
      LOGGER.debug(message) if SHOULD_LOG
    rescue StandardError
      LOGGER.debug('Unknown Error During Debug') if SHOULD_LOG
    end

    def self.expired_session(saml_attrs)
      LOGGER.info("Session Expired for #{saml_attrs[PRIMARY_ID]}") if SHOULD_LOG
    rescue StandardError
      LOGGER.debug('Unknown Error During relay state logging. Expired session check') if SHOULD_LOG
    end

    def self.logout(saml_attrs)
      LOGGER.info("#{saml_attrs[PRIMARY_ID]} has succesfully logged out.") if SHOULD_LOG
    rescue StandardError
      if SHOULD_LOG
        LOGGER.debug('Unknown error logging user logout.
 Most likely anonymous user clicked a logout button.')
      end
    end

    def self.saml_state(data)
      if SHOULD_LOG
        LOGGER.info("Stored Relay: #{data[:stored_relay]} |
  RequestRelay: #{data[:request_relay]} |
  Stored IP: #{data[:stored_ip]}  RemoteIP: #{data[:remote_ip]}")
      end
    rescue StandardError
      LOGGER.debug('Unknown Error During relay state logging. Saml state check') if SHOULD_LOG
    end

    def self.successful_auth(saml_attrs)
      LOGGER.info("#{saml_attrs[PRIMARY_ID]} has succesfully authenticated.") if SHOULD_LOG
    rescue StandardError
      if SHOULD_LOG
        LOGGER.debug('Unknown Error During successful_auth logging.
   Check PRIMARY_ID configured in settings.json and that user has attribute.')
      end
    end
  end
end
