# frozen_string_literal: true

require 'rubygems'
require 'onelogin/ruby-saml'
module SamlCamel
  # engine definition
  class Engine < ::Rails::Engine
    isolate_namespace SamlCamel
    config.to_prepare do
      ActionController::Base.include SamlCamel::SamlService
    end
  end
end
