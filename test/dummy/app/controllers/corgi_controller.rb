class CorgiController < ApplicationController
  protect_from_forgery with: :exception
  before_action :saml_protect, except: [:index]
  
  def index
  end

  def show
  end
end
